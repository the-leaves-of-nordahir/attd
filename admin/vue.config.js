let proxyObj = {}

proxyObj['/'] = {
  //  websocket
  ws: false,
  //  目标地址
  target: 'http://47.108.172.172:9000/attd',
  //target: 'http://localhost:9000/attd',
  //  发送请求头host会被设置target
  changeOrigin: true,
  //  不重写请求地址
  pathRewrite: {
    '^/': '/'
  }
}

//  通过node代理转发解决跨域
module.exports = {
  // devServer: {
  //   //  还是请求到8080端口，但是会通过proxy代理
  //   host: '47.108.172.172',
  //   port: '80',
  //   proxy: proxyObj
  // },
  publicPath: '/attd/'
}