import {getRequest} from "./api"

//  菜单请求工具类
export const initMenu = (router, store) => {
  if (store.state.routes.length > 0) {
    return
  }
  getRequest('/home/menu').then(data => {
    if (data) {
      //  格式化Router
      let fmtRoutes = formatRoutes(data)
      //  添加到router
      router.addRoutes(fmtRoutes)
      //  将数据存入vuex
      store.commit('initRoutes', fmtRoutes)
    }
  })
}

//  格式化
export const formatRoutes = (routes) => {
  let fmtRoutes = [];
  routes.forEach(router => {
    //  解构赋值, path = router.path, component = router.component, ...
    let {
      path,
      component,
      name,
      iconCls,
      children,
    } = router
    if (children && children instanceof Array) {
      //  递归
      children = formatRoutes(children)
    }
    let fmRouter = {
      path: path,
      name: name,
      iconCls: iconCls,
      children: children,
      component(resolve) {
        if (component.startsWith('Home')) {
          require(['../views/' + component + '.vue'], resolve)
        } else if (component.startsWith('Per')) {
          require(['../views/perinformation/' + component + '.vue'], resolve)
        } else if (component.startsWith('Sys')) {
          require(['../views/sys/' + component + '.vue'], resolve)
        } else if (component.startsWith('Attd')) {
          require(['../views/attdmanagement/' + component + '.vue'], resolve)
        } else if (component.startsWith('Edu')) {
          require(['../views/edumanagement/' + component + '.vue'], resolve)
        } else if (component.startsWith('Ann')) {
          require(['../views/annmanagement/' + component + '.vue'], resolve)
        } else if (component.startsWith('Lea')) {
          require(['../views/leamanagement/' + component + '.vue'], resolve)
        }
      }
    }
    fmtRoutes.push(fmRouter)
  })
  return fmtRoutes
}

