import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Router from 'vue-router'
import store from './store'
import ElementUI from 'element-ui'
import axios from 'axios'
import 'element-ui/lib/theme-chalk/index.css'
import 'font-awesome/css/font-awesome.css'
import echarts from 'echarts'


import {postRequest} from './utils/api'
import {putRequest} from './utils/api'
import {getRequest} from './utils/api'
import {deleteRequest} from './utils/api'
import {initMenu} from "./utils/menus"


Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$echarts = echarts

Vue.use(ElementUI, {size: 'medium'})
Vue.use(Router)

//  以插件形式全局引用， 使用前在方法前加上 this. (只能在.vue文件中使用，.js文件还是要自己引用)
Vue.prototype.postRequest = postRequest
Vue.prototype.putRequest = putRequest
Vue.prototype.getRequest = getRequest
Vue.prototype.deleteRequest = deleteRequest

//  路由导航守卫
router.beforeEach((to, from, next) => {
  if (window.sessionStorage.getItem('tokenStr')) {
    initMenu(router, store)
    // if (!window.sessionStorage.getItem('user')) {
    //   return getRequest('/admin/info').then(resp => {
    //     if (resp) {
    //       window.sessionStorage.setItem('user', JSON.stringify(resp))
    //       next()
    //     }
    //   })
    // }
    next()
  } else {
    //  如果未登录且进入登录页面就放行
    if (to.path === '/') {
      next()
    } else {
      //  如果未登录就访问页面
      next('/?redirect=' + to.path)
    }
  }
})

// 解决报错
const originalPush = Router.prototype.push
const originalReplace = Router.prototype.replace
// push
Router.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalReplace.call(this, location, onResolve, onReject)
  return originalReplace.call(this, location).catch(err => err)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
