package com.xxxx.server.config.security.component;

import com.xxxx.server.pojo.Menu;
import com.xxxx.server.pojo.Role;
import com.xxxx.server.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.List;

/**
 * 权限控制
 * 根据请求url分析请求所需的角色
 *
 * @author: STFU
 * @create: 2021-03-27
 **/

@Component
public class CustomerFilter implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private IMenuService menuService;
//    @Autowired(required = false) 不能用这个
//    private MenuMapper menuMapper;

    //  用于匹配路径
    AntPathMatcher antPathMatcher = new AntPathMatcher();

    //  根据请求的Url返回所需角色
    //  核心方法：getAttributes(Object object);其中object是一个类似Http Request的对象
    //  从object中获取客户端请求的Url，之后从数据库中查询出所有的菜单Url以及哪些角色可以访问对应的菜单Url

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //  获取请求的url
        String requestUrl = ((FilterInvocation) object).getRequestUrl();
        //  获取所有的菜单
        List<Menu> menus = menuService.getMenusWithRole();
        //  判断此请求的url需要哪些角色才能访问
        for(Menu menu : menus) {
            //  判断请求的url与菜单角色是否匹配
            if(antPathMatcher.match(menu.getUrl(), requestUrl)) {
                String[] str = menu.getRoles().stream().map(Role::getName).toArray(String[]::new);
                //  之后包装成SecurityConfig中的一个属性
                //  传递给CustomUrlDecisionManager的decide方法里的Collection<ConfigAttribute> configAttributes
                return SecurityConfig.createList(str);
            }
        }
        //  没匹配的url默认登录即可访问
        //  一些匹配不到的路径，由于目前的项目只有登录页面不需要登录就能访问，
        //  而其他Url都是需要登录后才能拿到  因此要创建一个“ROLE_LOGIN”角色来
        //  保证Url的权限。
        return SecurityConfig.createList("ROLE_LOGIN");
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
