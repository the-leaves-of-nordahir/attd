package com.xxxx.server.core;

import com.baidu.aip.face.AipFace;
import org.springframework.stereotype.Component;

@Component
public class AiFaceClient {
	private String APP_ID = "";
	private String API_KEY = "";
	private String SECRET_KEY = "";
	public String GROUD_LIST = "";

	private AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

	public AipFace getClient() {
		client.setConnectionTimeoutInMillis(2000);
		client.setSocketTimeoutInMillis(60000);
		return client;
	}
}
