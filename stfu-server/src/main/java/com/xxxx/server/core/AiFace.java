package com.xxxx.server.core;

import com.baidu.aip.face.AipFace;
import com.xxxx.server.pojo.Image;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author: STFU
 * @create: 2021-04-13
 **/
@Component
public class AiFace {

    @Autowired
    AiFaceClient aiFaceClient;

    public String multiSearch(Image image) {
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("max_face_num", "10");
        options.put("quality_control", "NORMAL");
        options.put("liveness_control", "LOW");
        options.put("max_user_num", "2");
        String groupIdList = aiFaceClient.GROUD_LIST;//人脸库组名称（需要自己修改）
        AipFace client = aiFaceClient.getClient();

        JSONObject res = client.multiSearch(image.getImgStr(), image.getImgType(), groupIdList, options);
        return res.toString(2);
    }

    public String getUser(String userId) {
        HashMap<String, String> options = new HashMap<String, String>();
        String groupIdList = aiFaceClient.GROUD_LIST;//人脸库组名称（需要自己修改）
        AipFace client = aiFaceClient.getClient();

        JSONObject res = client.getUser(userId, groupIdList, options);
        return res.toString(2);
    }

    public String faceGetList(String userId) {
        HashMap<String, String> options = new HashMap<String, String>();
        String groupIdList = aiFaceClient.GROUD_LIST;//人脸库组名称（需要自己修改）
        AipFace client = aiFaceClient.getClient();

        JSONObject res = client.faceGetlist(userId, groupIdList, options);
        return res.toString(2);
    }
}

