package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Announcement;
import com.xxxx.server.pojo.result.RespPageBean;

import java.time.LocalDate;

public interface IAnnouncementService extends IService<Announcement> {
    /**
     * 获取所有公告（分页）
     * @return
     * @param currentPage
     * @param size
     */
    RespPageBean getAllAnnouncementsByPage(Integer currentPage,
                                           Integer size,
                                           LocalDate[] dateScope);
}
