package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.StfuDepartmentMapper;
import com.xxxx.server.pojo.StfuDepartment;
import com.xxxx.server.service.IStfuDepartmentService;
import org.springframework.stereotype.Service;

/**
 * @author: STFU
 * @create: 2021-04-19
 **/
@Service
public class StfuDepartmentServiceImpl extends ServiceImpl<StfuDepartmentMapper, StfuDepartment> implements IStfuDepartmentService {
}