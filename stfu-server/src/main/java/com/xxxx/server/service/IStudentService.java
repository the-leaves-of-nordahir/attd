package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.pojo.Student;

import java.util.List;

public interface IStudentService extends IService<Student> {

    /**
     * 根据班级id获取学生
     * @param id
     * @return
     */
    List<Student> getStudentsByClassesId(Integer id);
    List<Student> getStudentsByClassesId();
    /**
     * 获取学生（分页）
     * @param currentPage
     * @param size
     * @param student
     * @return
     */
    RespPageBean getStudentsByPage(Integer currentPage, Integer size, Student student);

    /**
     * 添加学生
     * @param student
     * @return
     */
    RespBean addStudent(Student student);

    /**
     * 获取最大学号
     * @return
     */
    String maxSchoolID();

    /**
     * 根据学生id获取学生
     * @param id
     * @return
     */
    Student getStudentById(Integer id);
}
