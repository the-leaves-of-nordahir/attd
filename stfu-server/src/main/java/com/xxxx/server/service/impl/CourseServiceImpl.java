package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.CourseMapper;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Autowired(required = false)
    CourseMapper courseMapper;

    @Override
    public List<Course> getCoursesByTeacherId(Integer id) {
        return courseMapper.getCoursesByTeacherId(id);
    }
}
