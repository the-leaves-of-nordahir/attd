package com.xxxx.server.service;

import com.xxxx.server.pojo.Image;
import com.xxxx.server.pojo.result.RespBean;

public interface IAiFaceService {
    /**
     * M:N人脸识别
     * @param image
     * @return
     */
    RespBean multiSearch(Image image);

    RespBean getUser(String userId);

    RespBean faceGetList(String userId);
}
