package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.MenuMapper;
import com.xxxx.server.pojo.Menu;
import com.xxxx.server.service.IMenuService;
import com.xxxx.server.utils.AdminUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stfu
 * @since 2021-03-19
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired(required = false)
    private MenuMapper menuMapper;

    /**
     * 通过用户id查询菜单列表
     * @return
     */
    @Override
    public List<Menu> getMenusByAdminId() {
        //  通过springsecurity上下文查找用户信息
        Integer adminId = AdminUtils.getCurrentAdmin().getId();
//        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
//        //  先从redis获取菜单数据
//        List<Menu> menus = (List<Menu>) valueOperations.get("menu_" + adminId);
//        if (CollectionUtils.isEmpty(menus)) {
//            //  从数据库中获取数据
//            menus = menuMapper.getMenusByAdminId(adminId);
//            //  将数据保存至redis中
//            valueOperations.set("menu_" + adminId, menus);
//        }
        List<Menu> menus = menuMapper.getMenusByAdminId(adminId);
        return menus;
    }

    /**
     * 根据角色获取菜单列表
     * @return
     */
    @Override
    public List<Menu> getMenusWithRole() {
        return menuMapper.getMenusWithRole();
    }

    /**
     * 查询所有菜单
     * @return
     */
    @Override
    public List<Menu> getAllMenus() {
        return menuMapper.getAllMenus();
    }
}
