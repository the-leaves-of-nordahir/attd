package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Course;

import java.util.List;

public interface ICourseService extends IService<Course> {

    /**
     * 根据老师id获取课程
     * @param id
     * @return
     */
    List<Course> getCoursesByTeacherId(Integer id);
}

