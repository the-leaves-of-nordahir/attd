package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.StfuDepartment;

/**
 * @author: STFU
 * @create: 2021-04-19
 **/
public interface IStfuDepartmentService extends IService<StfuDepartment> {
}
