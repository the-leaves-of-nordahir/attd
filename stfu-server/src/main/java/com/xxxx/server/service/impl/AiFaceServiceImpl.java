package com.xxxx.server.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xxxx.server.core.AiFaceClient;
import com.xxxx.server.core.AiFace;
import com.xxxx.server.pojo.Image;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.service.IAiFaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 人脸识别
 *
 * @author: STFU
 * @create: 2021-04-14
 **/
@Service
public class AiFaceServiceImpl implements IAiFaceService {

    @Autowired
    AiFaceClient aiFaceClient;
    @Autowired
    AiFace aiFace;

    @Override
    public RespBean multiSearch(Image image) {
        String res = aiFace.multiSearch(image);
        JSONObject result = JSONObject.parseObject(res);
        String error_code = result.getString("error_code");
        String error_msg = result.getString("error_msg");
        if (0 == Integer.parseInt(error_code)) {
            JSONArray face_list = result.getJSONObject("result")
                    .getJSONArray("face_list");
            return RespBean.success("识别成功！", face_list);
        } else {
            return RespBean.error(error_msg);
        }
    }

    

    @Override
    public RespBean getUser(String userId) {
        String res = aiFace.getUser(userId);
        JSONObject result = JSONObject.parseObject((res));
        return RespBean.success("成功", result);
    }

    @Override
    public RespBean faceGetList(String userId) {
        String res = aiFace.faceGetList(userId);
        JSONObject result = JSONObject.parseObject((res));
        return RespBean.success("成功", result);
    }


//222202	pic not has face	图片中没有人脸	检查图片质量
//222203	image check fail	无法解析人脸	检查图片质量
//222207	match user is not found	未找到匹配的用户	请确认人脸库中 是否存在此用户
//223113	face is covered	人脸有被遮挡	提示用户请勿遮挡面部
//223114	face is fuzzy	人脸模糊	人脸图片模糊，前端页面可以提示用户拍摄时不要晃动手机
//223115	face light is not good	人脸光照不好	提示用户到光线适宜的地方拍摄
//223116	incomplete face	人脸不完整	提示用户请勿遮挡面部
}
