package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.TeacherCourse;

public interface ITeacherCourseService extends IService<TeacherCourse> {
    /**
     * 更新或添加课程与老师的关联
     * @param courseId
     * @param teacherIds
     * @return
     */
    RespBean updateCourseWithTeachers(Integer courseId, Integer[] teacherIds);
}