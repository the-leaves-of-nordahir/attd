package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.TeacherCourseMapper;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.TeacherCourse;
import com.xxxx.server.service.ITeacherCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: STFU
 * @create: 2021-04-19
 **/
@Service
public class TeacherCourseServiceImpl extends ServiceImpl<TeacherCourseMapper, TeacherCourse> implements ITeacherCourseService {

    @Autowired(required = false)
    TeacherCourseMapper teacherCourseMapper;

    @Override
    @Transactional
    public RespBean updateCourseWithTeachers(Integer courseId, Integer[] teacherIds) {
        teacherCourseMapper.delete(new QueryWrapper<TeacherCourse>().eq("course_id", courseId));
        if (null == teacherIds || 0 == teacherIds.length) {
            return RespBean.success("更新成功！");
        }
        Integer result = teacherCourseMapper.insertCourseWithTeachers(courseId, teacherIds);
        if (result == teacherIds.length) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }
}