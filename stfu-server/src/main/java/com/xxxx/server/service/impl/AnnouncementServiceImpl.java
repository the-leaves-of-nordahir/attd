package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.AnnouncementMapper;
import com.xxxx.server.pojo.Announcement;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * @author: STFU
 * @create: 2021-04-23
 **/
@Service
public class AnnouncementServiceImpl extends ServiceImpl<AnnouncementMapper, Announcement> implements IAnnouncementService {

    @Autowired(required = false)
    private AnnouncementMapper announcementMapper;

    @Override
    public RespPageBean getAllAnnouncementsByPage(Integer currentPage,
                                                  Integer size,
                                                  LocalDate[] dateScope) {
        Page<Announcement> page = new Page<>(currentPage, size);
        IPage<Announcement> AnnouncementsByPage = announcementMapper.getAllAnnouncementsByPage(page, dateScope);
        //  自定义分页返回实体类
        RespPageBean respPageBean = new RespPageBean(AnnouncementsByPage.getTotal(), AnnouncementsByPage.getRecords());
        return respPageBean;
    }
}
