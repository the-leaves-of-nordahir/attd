package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Teacher;

import java.util.List;

public interface ITeacherService extends IService<Teacher> {
    /**
     * 获取所有老师
     * @return
     */
    List<Teacher> getAllTeachers();

    /**
     * 根据课程id获取课程对应老师列表
     * @param id
     * @return
     */
    List<Teacher> getTeachersByCourseId(Integer id);

    /**
     * 根据id获取老师
     * @param id
     * @return
     */
    Teacher getTeacherById(Integer id);
}