package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Classes;
import com.xxxx.server.pojo.result.RespPageBean;

public interface IClassesService extends IService<Classes> {
    /**
     * 获取所有班级
     * @return
     */
    RespPageBean getAllClassesByPage(Integer currentPage, Integer size, Classes classes);
}
