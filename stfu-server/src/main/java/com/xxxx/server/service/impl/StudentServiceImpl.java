package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.StudentMapper;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.pojo.Student;
import com.xxxx.server.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    @Autowired(required = false)
    private StudentMapper studentMapper;

    @Override
    public List<Student> getStudentsByClassesId(Integer id) {
        return studentMapper.getStudentsByClassesId(id);
    }

    @Override
    public List<Student> getStudentsByClassesId() {
        return studentMapper.getStudentsByClassesId();
    }

    @Override
    public RespPageBean getStudentsByPage(Integer currentPage, Integer size, Student student) {
        Page<Student> page = new Page<>(currentPage, size);
        IPage<Student> studentsByPage = studentMapper.getStudentsByPage(page, student);
        //  自定义分页返回实体类
        RespPageBean respPageBean = new RespPageBean(studentsByPage.getTotal(), studentsByPage.getRecords());
        return respPageBean;
    }

    @Override
    public RespBean addStudent(Student student) {
        if (1 == studentMapper.insert(student)) {
            RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @Override
    public String maxSchoolID() {
        String max = studentMapper.maxSchoolID();
        return String.format("%08d", Integer.parseInt(max) + 1);
    }

    @Override
    public Student getStudentById(Integer id) {
        return studentMapper.getStudentById(id);
    }
}
