package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.AttdRecordMapper;
import com.xxxx.server.mapper.MailLogMapper;
import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.MailConstants;
import com.xxxx.server.pojo.MailLog;
import com.xxxx.server.pojo.result.CourseStatistics;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.pojo.result.StudentStatistics;
import com.xxxx.server.service.IAttdRecordService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
@Service
public class AttdRecordServiceImpl extends ServiceImpl<AttdRecordMapper, AttdRecord> implements IAttdRecordService {

    @Autowired(required = false)
    private AttdRecordMapper attdRecordMapper;
    @Autowired(required = false)
    private MailLogMapper mailLogMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public List<StudentStatistics> getStatistics(AttdRecord attdRecord) {
        List<StudentStatistics> studentStatistics = new ArrayList<>();
        List<AttdRecord> students = attdRecordMapper.getDistinctStudents(attdRecord);
        for (AttdRecord student : students) {
            int studentId = student.getStudentId();
            studentStatistics.add(attdRecordMapper.getStatistics(attdRecord, studentId));
        }
        return studentStatistics;
    }

    @Override
    public CourseStatistics getCourseStatistics(AttdRecord attdRecord) {
        return attdRecordMapper.getCourseStatistics(attdRecord);
    }

    @Override
    public RespPageBean getRecordsByPage(Integer currentPage, Integer size, AttdRecord attdRecord) {
        Page<AttdRecord> page = new Page<>(currentPage, size);
        IPage<AttdRecord> recordsByPage = attdRecordMapper.getRecordsByPage(page, attdRecord);
        //  自定义分页返回实体类
        RespPageBean respPageBean = new RespPageBean(recordsByPage.getTotal(), recordsByPage.getRecords());
        return respPageBean;
    }

    @Override
    public RespPageBean getLeaveByPage(Integer currentPage, Integer size, AttdRecord attdRecord) {
        Page<AttdRecord> page = new Page<>(currentPage, size);
        IPage<AttdRecord> leaveByPage = attdRecordMapper.getLeaveByPage(page, attdRecord);
        //  自定义分页返回实体类
        RespPageBean respPageBean = new RespPageBean(leaveByPage.getTotal(), leaveByPage.getRecords());
        return respPageBean;
    }

    @Override
    public RespBean addLeave(AttdRecord attdRecord) {
        if (1 == attdRecordMapper.insert(attdRecord)) {
            AttdRecord leave = attdRecordMapper.getLeaveById(attdRecord.getId());

            //  数据库记录发送的消息（消息落户）
            String msgId = UUID.randomUUID().toString();
            MailLog maillog = new MailLog();
            maillog.setMsgId(msgId);
            maillog.setLeaveId(attdRecord.getId());
            maillog.setStatus(0);
            maillog.setRouteKey(MailConstants.MAIL_ROUTING_KEY_NAME);
            maillog.setExchange(MailConstants.MAIL_EXCHANGE_NAME);
            maillog.setCount(0);
            //  重试时间，加一分钟
            maillog.setTryTime(LocalDateTime.now().plusMinutes(MailConstants.MSG_TIMEOUT));
            maillog.setCreateTime(LocalDateTime.now());
            maillog.setUpdateTime(LocalDateTime.now());
            //  数据库插入数据
            mailLogMapper.insert(maillog);

            rabbitTemplate.convertAndSend(MailConstants.MAIL_EXCHANGE_NAME,
                    MailConstants.MAIL_ROUTING_KEY_NAME, leave, new CorrelationData(msgId));
            return RespBean.success("申请成功！");
        }
        return RespBean.error("申请失败！");
    }

    @Override
    public AttdRecord getLeaveById(Integer id) {
        return attdRecordMapper.getLeaveById(id);
    }

}
