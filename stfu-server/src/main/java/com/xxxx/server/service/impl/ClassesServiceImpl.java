package com.xxxx.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxx.server.mapper.ClassesMapper;
import com.xxxx.server.pojo.Classes;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements IClassesService {

    @Autowired(required = false)
    ClassesMapper classesMapper;

    @Override
    public RespPageBean getAllClassesByPage(Integer currentPage, Integer size, Classes classes) {
        Page<Classes> page = new Page<>(currentPage, size);
        IPage<Classes> classesByPage = classesMapper.getAllClassesByPage(page);
        //  自定义分页返回实体类
        RespPageBean respPageBean = new RespPageBean(classesByPage.getTotal(), classesByPage.getRecords());
        return respPageBean;
    }
}
