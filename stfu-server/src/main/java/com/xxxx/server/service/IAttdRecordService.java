package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.result.CourseStatistics;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.pojo.result.StudentStatistics;

import java.util.List;

public interface IAttdRecordService extends IService<AttdRecord> {

    /**
     * 获取考勤统计(学生)
     * @param attdRecord
     * @return
     */
    List<StudentStatistics> getStatistics(AttdRecord attdRecord);

    /**
     * 获取考勤统计(课程)
     * @param attdRecord
     * @return
     */
    CourseStatistics getCourseStatistics(AttdRecord attdRecord);

    /**
     * 获取考勤记录
     *
     * @param currentPage
     * @param size
     * @param attdRecord
     * @return
     */
    RespPageBean getRecordsByPage(Integer currentPage, Integer size, AttdRecord attdRecord);

    /**
     * 获取请假记录(分页)
     *
     * @param currentPage
     * @param size
     * @param attdRecord
     * @return
     */
    RespPageBean getLeaveByPage(Integer currentPage, Integer size, AttdRecord attdRecord);

    /**
     * 添加请假记录
     * @param attdRecord
     * @return
     */
    RespBean addLeave(AttdRecord attdRecord);

    /**
     * 根据id获取请假记录
     * @param id
     * @return
     */
    AttdRecord getLeaveById(Integer id);
}
