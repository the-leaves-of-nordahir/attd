package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Major;

import java.util.List;

public interface IMajorService extends IService<Major> {
    /**
     * 获取所有专业
     * @return
     */
    List<Major> getAllMajors();
}
