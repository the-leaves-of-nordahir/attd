package com.xxxx.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 学生
 * @author: STFU
 * @create: 2021-04-14
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stfu_student")
@ApiModel(value="Student对象", description="")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生姓名")
    private String name;

    @ApiModelProperty(value = "班级id")
    @TableField("classes_id")
    private Integer classesId;

    @ApiModelProperty(value = "学院id")
    @TableField("department_id")
    private Integer departmentId;

    @ApiModelProperty(value = "专业id")
    @TableField("major_id")
    private Integer majorId;

    @ApiModelProperty(value = "考勤状态")
    private Boolean status;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "宿舍")
    private String dormitory;

    @ApiModelProperty(value = "学号")
    @TableField("school_id")
    private String schoolID;

    @ApiModelProperty(value = "头像")
    @TableField("face")
    private String face;

    //---------------------------------------------外键id---------------------------------------------

    @ApiModelProperty(value = "班级")
    @TableField(exist = false)
    private Classes classes;

    @ApiModelProperty(value = "专业")
    @TableField(exist = false)
    private Major major;

    @ApiModelProperty(value = "学院")
    @TableField(exist = false)
    private StfuDepartment department;

}
