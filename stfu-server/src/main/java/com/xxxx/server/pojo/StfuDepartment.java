package com.xxxx.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 学院（跟yeb的 department 部门 重名了）
 *
 * @author: STFU
 * @create: 2021-04-19
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stfu_department")
@ApiModel(value="StfuDepartment对象", description="")
public class StfuDepartment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学院名字")
    private String name;

    @ApiModelProperty(value = "学院院长")
    private String dean;

    @ApiModelProperty(value = "学院党委书记")
    private String secretary;

    @ApiModelProperty(value = "学院办公电话")
    private String phone;

}