package com.xxxx.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 考勤记录
 *
 * @author: STFU
 * @create: 2021-04-14
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stfu_attd_record")
@ApiModel(value="AttdRecord对象", description="")
public class AttdRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    @TableField("student_id")
    private Integer studentId;

    @ApiModelProperty(value = "学生姓名")
    @TableField("student_name")
    private String studentName;

    @ApiModelProperty(value = "课程id")
    @TableField("course_id")
    private Integer courseId;

    @ApiModelProperty(value = "老师id")
    @TableField("teacher_id")
    private Integer teacherId;

    @ApiModelProperty(value = "考勤状态Enum")
    private String status;

    @ApiModelProperty(value = "考勤记录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    @TableField("record_time")
    private LocalDateTime recordTime;

    @ApiModelProperty(value = "请假理由")
    private String reason;

    @ApiModelProperty(value = "教师留言")
    private String message;

    @ApiModelProperty(value = "请假进度")
    private Integer progress;

    @ApiModelProperty(value = "请假时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Shanghai")
    @TableField("leave_time")
    private LocalDate leaveTime;

    //---------------------------------------------外键id---------------------------------------------

    @ApiModelProperty(value = "学生")
    @TableField(exist = false)
    private Student student;

    @ApiModelProperty(value = "课程")
    @TableField(exist = false)
    private Course course;

    @ApiModelProperty(value = "老师")
    @TableField(exist = false)
    private Teacher teacher;

    //---------------------------------------------自定义属性---------------------------------------------

    @ApiModelProperty(value = "学生出勤")
    @TableField(exist = false)
    private Integer studentAttd;

    @ApiModelProperty(value = "学生旷课")
    @TableField(exist = false)
    private Integer studentLate;

    @ApiModelProperty(value = "学生请假")
    @TableField(exist = false)
    private Integer studentLeave;

    @ApiModelProperty(value = "课程总出勤")
    @TableField(exist = false)
    private Integer courseAttd;

    @ApiModelProperty(value = "课程总旷课")
    @TableField(exist = false)
    private Integer courseLate;

    @ApiModelProperty(value = "课程总请假")
    @TableField(exist = false)
    private Integer courseLeave;

}
