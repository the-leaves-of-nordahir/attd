package com.xxxx.server.pojo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 课程统计类
 *
 * @author: STFU
 * @create: 2021-04-28
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseStatistics {

    private Integer courseAttd;

    private Integer courseLate;

    private Integer courseLeave;

}
