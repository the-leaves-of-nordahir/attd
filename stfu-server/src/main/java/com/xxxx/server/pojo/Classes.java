package com.xxxx.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级
 *
 * @author: STFU
 * @create: 2021-04-14
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stfu_classes")
@ApiModel(value="Classes对象", description="")
public class Classes implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "班级姓名")
    private String name;

    @ApiModelProperty(value = "专业id")
    @TableField("major_id")
    private Integer majorId;

    //---------------------------------------------外键id---------------------------------------------

    @ApiModelProperty(value = "专业")
    @TableField(exist = false)
    private Major major;

}
