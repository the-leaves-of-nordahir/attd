package com.xxxx.server.pojo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 考勤统计返回对象
 *
 * @author: STFU
 * @create: 2021-04-27
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespStatisticsBean {

    private List<StudentStatistics> studentStatistics;

}
