package com.xxxx.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 专业
 *
 * @author: STFU
 * @create: 2021-04-19
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stfu_major ")
@ApiModel(value="Major对象", description="")
public class Major implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "专业名字")
    private String name;

    @ApiModelProperty(value = "学院id")
    @TableField("department_id")
    private Integer departmentId;

    @ApiModelProperty(value = "系主任名字")
    private String dean;

    @ApiModelProperty(value = "办公电话")
    private String phone;

    //---------------------------------------------外键id---------------------------------------------

    @ApiModelProperty(value = "学院")
    @TableField(exist = false)
    private StfuDepartment department;

}
