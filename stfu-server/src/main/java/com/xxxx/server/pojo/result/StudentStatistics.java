package com.xxxx.server.pojo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生统计类
 *
 * @author: STFU
 * @create: 2021-04-27
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentStatistics {

    private String studentName;

    private Integer studentLate;

    private Integer studentAttd;

    private Integer studentLeave;
}
