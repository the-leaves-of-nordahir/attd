package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.result.CourseStatistics;
import com.xxxx.server.pojo.result.StudentStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
public interface AttdRecordMapper extends BaseMapper<AttdRecord> {

    /**
     * 获取去重后的学生
     * @param attdRecord
     * @return
     */
    List<AttdRecord> getDistinctStudents(@Param("attdRecord") AttdRecord attdRecord);

    /**
     * 获取考勤统计(学生)
     * @param attdRecord
     * @return
     */
    StudentStatistics getStatistics(@Param("attdRecord") AttdRecord attdRecord, @Param("studentId") Integer studentId);

    /**
     * 获取考勤统计(课程)
     * @param attdRecord
     * @return
     */
    CourseStatistics getCourseStatistics(@Param("attdRecord") AttdRecord attdRecord);

    /**
     * 获取考勤记录
     * @param attdRecord
     * @return
     */
    List<AttdRecord> getRecordsByTeacherId(@Param("attdRecord") AttdRecord attdRecord);

    /**
     * 获取考勤记录(分页)
     * @param attdRecord
     * @return
     */
    IPage<AttdRecord> getRecordsByPage(Page<AttdRecord> page, @Param("attdRecord") AttdRecord attdRecord);

    /**
     * 获取请假记录(分页)
     * @param page
     * @param attdRecord
     * @return
     */
    IPage<AttdRecord> getLeaveByPage(Page<AttdRecord> page, @Param("attdRecord") AttdRecord attdRecord);

    AttdRecord getLeaveById(Integer id);
}
