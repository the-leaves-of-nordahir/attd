package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.Course;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-14
 **/
public interface CourseMapper extends BaseMapper<Course> {

    /**
     * 根据老师id获取课程
     * @param id
     * @return
     */
    List<Course> getCoursesByTeacherId(Integer id);
}
