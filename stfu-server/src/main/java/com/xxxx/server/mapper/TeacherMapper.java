package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.Teacher;

import java.util.List;

public interface TeacherMapper extends BaseMapper<Teacher> {
    /**
     * 获取所有老师
     *
     * @return
     */
    List<Teacher> getAllTeachers();

    /**
     * 根据课程id获取课程对应老师列表
     *
     * @param id
     * @return
     */
    List<Teacher> getTeachersByCourseId(Integer id);

    /**
     * 根据id获取老师
     * @param id
     * @return
     */
    Teacher getTeacherById(Integer id);
}
