package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxxx.server.pojo.Classes;

public interface ClassesMapper extends BaseMapper<Classes> {
    /**
     * 获取所有班级
     * @return
     */
    IPage<Classes> getAllClassesByPage(Page<Classes> page);
}
