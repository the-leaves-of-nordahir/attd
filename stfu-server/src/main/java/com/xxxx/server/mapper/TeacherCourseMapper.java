package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.TeacherCourse;
import org.apache.ibatis.annotations.Param;

public interface TeacherCourseMapper extends BaseMapper<TeacherCourse> {
    /**
     * 更新或添加课程与老师的关联
     * @param courseId
     * @param teacherIds
     * @return
     */
    Integer insertCourseWithTeachers(@Param("courseId") Integer courseId,
                                     @Param("teacherIds") Integer[] teacherIds);
}
