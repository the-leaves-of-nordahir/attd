package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxxx.server.pojo.Announcement;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;

public interface AnnouncementMapper extends BaseMapper<Announcement> {
    /**
     * 获取所有公告（分页）
     * @param page
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<Announcement> getAllAnnouncementsByPage(Page<Announcement> page,
                                                  @Param("dateScope") LocalDate[] dateScope);
}
