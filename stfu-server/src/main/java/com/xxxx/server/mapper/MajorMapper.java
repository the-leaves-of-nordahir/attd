package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.Major;

import java.util.List;

public interface MajorMapper extends BaseMapper<Major> {
    /**
     * 获取所有专业
     * @return
     */
    List<Major> getAllMajors();
}
