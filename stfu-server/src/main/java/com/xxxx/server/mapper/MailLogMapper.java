package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.MailLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author stfu
 * @since 2021-03-19
 */
public interface MailLogMapper extends BaseMapper<MailLog> {

}
