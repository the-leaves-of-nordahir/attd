package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.StfuDepartment;

public interface StfuDepartmentMapper extends BaseMapper<StfuDepartment> {
}
