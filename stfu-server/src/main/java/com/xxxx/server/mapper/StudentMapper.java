package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxxx.server.pojo.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentMapper extends BaseMapper<Student> {

    List<Student> getStudentsByClassesId(Integer id);
    List<Student> getStudentsByClassesId();

    IPage<Student> getStudentsByPage(Page<Student> page,
                                     @Param("student") Student student);

    /**
     * 获取最大学号
     * @return
     */
    String maxSchoolID();

    /**
     * 根据学生id获取学生
     * @param id
     * @return
     */
    Student getStudentById(Integer id);
}
