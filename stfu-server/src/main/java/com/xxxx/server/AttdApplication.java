package com.xxxx.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 *
 * @author: STFU
 * @create: 2021-03-19
 **/

@SpringBootApplication
@MapperScan("com.xxxx.server.mapper")
@EnableScheduling // 开启定时任务
@EnableDiscoveryClient
public class AttdApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttdApplication.class, args);
    }
}
