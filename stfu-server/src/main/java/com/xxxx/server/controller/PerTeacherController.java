package com.xxxx.server.controller;

import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.Teacher;
import com.xxxx.server.service.ICourseService;
import com.xxxx.server.service.IStfuDepartmentService;
import com.xxxx.server.service.ITeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 老师信息
 *
 * @author: STFU
 * @create: 2021-05-04
 **/
@RestController
@RequestMapping("/perinformation/teacher")
public class PerTeacherController {

    @Autowired
    ITeacherService teacherService;
    @Autowired
    ICourseService courseService;
    @Autowired
    IStfuDepartmentService stfuDepartmentService;

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取老师")
    public Teacher getTeacherById(@PathVariable Integer id) {
        return teacherService.getTeacherById(id);
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

}
