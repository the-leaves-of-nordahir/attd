package com.xxxx.server.controller;

import com.xxxx.server.pojo.Student;
import com.xxxx.server.service.IStudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 学生信息
 *
 * @author: STFU
 * @create: 2021-05-04
 **/
@RestController
@RequestMapping("/perinformation/student")
public class PerStudentController {

    @Autowired
    private IStudentService studentService;

    @GetMapping("/{id}")
    @ApiOperation(value = "根据学生id获取学生")
    public Student getStudentById(@PathVariable Integer id) {
        return studentService.getStudentById(id);
    }

}
