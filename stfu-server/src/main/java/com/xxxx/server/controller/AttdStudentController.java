package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import com.xxxx.server.service.IStudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的考勤(学生)
 *
 * @author: STFU
 * @create: 2021-05-05
 **/
@RestController
@RequestMapping("/attdmanagement/student")
public class AttdStudentController {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private ICourseService courseService;
    @Autowired
    private IAttdRecordService attdRecordService;

    @GetMapping("/")
    @ApiOperation(value = "获取考勤记录(分页)")
    public RespPageBean getRecordsByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                         @RequestParam(defaultValue = "10") Integer size,
                                         AttdRecord attdRecord) {
        return attdRecordService.getRecordsByPage(currentPage, size, attdRecord);
    }

    @GetMapping("/course")
    @ApiOperation(value = "获取所有课程")
    public List<Course> getAllCourses() {
        return courseService.list();
    }

}
