package com.xxxx.server.controller;

import com.xxxx.server.pojo.*;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IClassesService;
import com.xxxx.server.service.IMajorService;
import com.xxxx.server.service.IStfuDepartmentService;
import com.xxxx.server.service.IStudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-19
 **/
@RestController
@RequestMapping("/edumanagement/student")
public class EduStudentManagementController {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private IStfuDepartmentService stfuDepartmentService;
    @Autowired
    private IMajorService majorService;
    @Autowired
    private IClassesService classesService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有学生（分页）")
    public RespPageBean getStudentsByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                          @RequestParam(defaultValue = "10") Integer size,
                                          Student student) {
        return studentService.getStudentsByPage(currentPage, size, student);
    }

    @PostMapping("/")
    @ApiOperation(value = "添加学生")
    public RespBean addStudent(@RequestBody Student student) {
        if (studentService.save(student)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/")
    @ApiOperation(value = "更新学生")
    public RespBean updateStudent(@RequestBody Student student) {
        if (studentService.updateById(student)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除学生")
    public RespBean deleteStudent(@PathVariable Integer id) {
        if (studentService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @ApiOperation(value = "批量删除学生")
    @DeleteMapping("/")
    public RespBean deleteStudentsByIds(Integer[] ids) {
        if(studentService.removeByIds(Arrays.asList(ids))) {
            return RespBean.success("批量删除成功！");
        }
        return RespBean.error("批量删除失败！");
    }

    @GetMapping("/maxSchoolID")
    @ApiOperation(value = "获取最大学号")
    public String maxSchoolID() {
        return studentService.maxSchoolID();
    }

    @GetMapping("/department")
    @ApiOperation(value = "获取所有学院")
    public List<StfuDepartment> getAllDepartments() {
        return stfuDepartmentService.list();
    }

    @GetMapping("/major")
    @ApiOperation(value = "获取所有专业")
    public List<Major> getAllMajors() {
        return majorService.list();
    }

    @GetMapping("/classes")
    @ApiOperation(value = "获取所有班级")
    public List<Classes> getAllClasses() {
        return classesService.list();
    }

}
