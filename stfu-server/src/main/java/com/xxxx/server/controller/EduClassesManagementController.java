package com.xxxx.server.controller;

import com.xxxx.server.pojo.Classes;
import com.xxxx.server.pojo.Major;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IClassesService;
import com.xxxx.server.service.IMajorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-22
 **/
@RestController
@RequestMapping("/edumanagement/classes")
public class EduClassesManagementController {

    @Autowired
    private IClassesService classesService;
    @Autowired
    private IMajorService majorService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有班级")
    public RespPageBean getAllClassesByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                            @RequestParam(defaultValue = "10") Integer size,
                                            Classes classes) {
        return classesService.getAllClassesByPage(currentPage, size, classes);
    }

    @PostMapping("/")
    @ApiOperation(value = "添加班级")
    public RespBean addClasses(@RequestBody Classes classes) {
        if (classesService.save(classes)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/")
    @ApiOperation(value = "更新班级")
    public RespBean updateClasses(@RequestBody Classes classes) {
        if (classesService.updateById(classes)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除班级")
    public RespBean deleteClasses(@PathVariable Integer id) {
        if (classesService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @ApiOperation(value = "批量删除班级")
    @DeleteMapping("/")
    public RespBean deleteClassesByIds(Integer[] ids) {
        if(classesService.removeByIds(Arrays.asList(ids))) {
            return RespBean.success("批量删除成功！");
        }
        return RespBean.error("批量删除失败！");
    }

    @GetMapping("/major")
    @ApiOperation(value = "获取所有专业")
    public List<Major> getAllMajors() {
        return majorService.list();
    }

}
