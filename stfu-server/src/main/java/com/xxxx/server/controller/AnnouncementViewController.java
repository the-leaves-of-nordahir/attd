package com.xxxx.server.controller;

import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAnnouncementService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * 查看公告
 *
 * @author: STFU
 * @create: 2021-04-24
 **/
@RestController
@RequestMapping("/annmanagement/annview")
public class AnnouncementViewController {

    @Autowired
    private IAnnouncementService announcementService;

    @GetMapping("/")
    @ApiOperation(value = "获取公告")
    public RespPageBean getAllAnnouncementsByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                  @RequestParam(defaultValue = "10") Integer size,
                                                  LocalDate[] dateScope) {

        return announcementService.getAllAnnouncementsByPage(currentPage, size, dateScope);
    }

}
