package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.Teacher;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import com.xxxx.server.service.ITeacherCourseService;
import com.xxxx.server.service.ITeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 请假申请
 *
 * @author: STFU
 * @create: 2021-05-02
 **/
@RestController
@RequestMapping("/leamanagement/apply")
public class LeaApplyController {
    
    @Autowired
    IAttdRecordService attdRecordService;
    @Autowired
    ICourseService courseService;
    @Autowired
    ITeacherCourseService teacherCourseService;
    @Autowired
    ITeacherService teacherService;


    @GetMapping("/")
    @ApiOperation(value = "获取请假记录")
    public RespPageBean getAllLeaveByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                          @RequestParam(defaultValue = "10") Integer size,
                                          AttdRecord attdRecord) {
        return attdRecordService.getLeaveByPage(currentPage, size, attdRecord);
    }

    @PostMapping("/")
    @ApiOperation(value = "添加请假记录")
    public RespBean addLeave(@RequestBody AttdRecord attdRecord) {
        return attdRecordService.addLeave(attdRecord);
    }

//    @PostMapping("/")
//    @ApiOperation(value = "添加请假记录")
//    public RespBean addLeave(@RequestBody AttdRecord attdRecord) {
//        if (attdRecordService.save(attdRecord)) {
//            return RespBean.success("添加成功！");
//        }
//        return RespBean.error("添加失败！");
//    }

    @GetMapping("/course")
    @ApiOperation(value = "获取所有课程")
    public List<Course> getAllCourses() {
        return courseService.list();
    }

    @GetMapping("/teacher/{id}")
    @ApiOperation(value = "根据课程id获取课程对应老师列表")
    public List<Teacher> getTeachersByCourseId(@PathVariable Integer id) {
        return teacherService.getTeachersByCourseId(id);
    }

    @GetMapping("/teacher")
    @ApiOperation(value = "获取所有老师")
    public List<Teacher> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }
}
