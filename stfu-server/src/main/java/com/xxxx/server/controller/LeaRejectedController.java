package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 请假处理-已拒绝
 *
 * @author: STFU
 * @create: 2021-05-04
 **/
@RestController
@RequestMapping("/leamanagement/handle/rejected")
public class LeaRejectedController {

    @Autowired
    IAttdRecordService attdRecordService;
    @Autowired
    ICourseService courseService;

    @GetMapping("/")
    @ApiOperation(value = "获取请假记录")
    public RespPageBean getAllLeaveByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                          @RequestParam(defaultValue = "10") Integer size,
                                          AttdRecord attdRecord) {
        return attdRecordService.getLeaveByPage(currentPage, size, attdRecord);
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

}
