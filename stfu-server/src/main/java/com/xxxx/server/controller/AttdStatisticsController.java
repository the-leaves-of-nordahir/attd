package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.CourseStatistics;
import com.xxxx.server.pojo.result.StudentStatistics;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 考勤统计
 *
 * @author: STFU
 * @create: 2021-04-27
 **/
@RestController
@RequestMapping("/attdmanagement/statistics")
public class AttdStatisticsController {

    @Autowired
    private ICourseService courseService;

    @Autowired
    private IAttdRecordService attdRecordService;

    @GetMapping("/student")
    @ApiOperation(value = "根据课程id与老师id获取统计结果(学生)")
    public List<StudentStatistics> getStatistics(AttdRecord attdRecord) {
        return attdRecordService.getStatistics(attdRecord);
    }

    @GetMapping("/course")
    @ApiOperation(value = "根据课程id与老师id获取统计结果(课程)")
    public CourseStatistics getCourseStatistics(AttdRecord attdRecord) {
        return attdRecordService.getCourseStatistics(attdRecord);
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }
}
