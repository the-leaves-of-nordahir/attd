package com.xxxx.server.controller;

import com.xxxx.server.pojo.Announcement;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.service.IAnnouncementService;
import com.xxxx.server.service.IClassesService;
import com.xxxx.server.service.IMajorService;
import com.xxxx.server.service.IStfuDepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发布公告
 *
 * @author: STFU
 * @create: 2021-04-23
 **/
@RestController
@RequestMapping("/annmanagement/annadd")
public class AnnouncementAddController {

    @Autowired
    private IAnnouncementService announcementService;
    @Autowired
    private IStfuDepartmentService stfuDepartmentService;
    @Autowired
    private IMajorService majorService;
    @Autowired
    private IClassesService classesService;

    @PostMapping("/")
    @ApiOperation(value = "添加公告")
    public RespBean addAnnouncement(@RequestBody Announcement announcement) {
        if (announcementService.save(announcement)) {
            return RespBean.success("发布成功！");
        }
        return RespBean.error("发布失败！");
    }

}
