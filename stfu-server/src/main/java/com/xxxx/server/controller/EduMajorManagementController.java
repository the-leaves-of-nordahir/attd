package com.xxxx.server.controller;

import com.xxxx.server.pojo.Major;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.StfuDepartment;
import com.xxxx.server.service.IMajorService;
import com.xxxx.server.service.IStfuDepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-20
 **/
@RestController
@RequestMapping("/edumanagement/major")
public class EduMajorManagementController {

    @Autowired
    private IMajorService majorService;
    @Autowired
    private IStfuDepartmentService stfuDepartmentService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有专业")
    public List<Major> getAllMajors() {
        return majorService.getAllMajors();
    }

    @PostMapping("/")
    @ApiOperation(value = "添加专业")
    public RespBean addMajor(@RequestBody Major major) {
        if (majorService.save(major)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/")
    @ApiOperation(value = "更新专业")
    public RespBean updateMajor(@RequestBody Major major) {
        if (majorService.updateById(major)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除专业")
    public RespBean deleteMajor(@PathVariable Integer id) {
        if (majorService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @ApiOperation(value = "批量删除专业")
    @DeleteMapping("/")
    public RespBean deleteMajorsByIds(Integer[] ids) {
        if(majorService.removeByIds(Arrays.asList(ids))) {
            return RespBean.success("批量删除成功！");
        }
        return RespBean.error("批量删除失败！");
    }

    @GetMapping("/department")
    @ApiOperation(value = "获取所有学院")
    public List<StfuDepartment> getAllDepartments() {
        return stfuDepartmentService.list();
    }

}
