package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 请假处理-待处理
 *
 * @author: STFU
 * @create: 2021-05-03
 **/
@RestController
@RequestMapping("/leamanagement/handle/pending")
public class LeaPendingController {

    @Autowired
    IAttdRecordService attdRecordService;
    @Autowired
    ICourseService courseService;

    @GetMapping("/")
    @ApiOperation(value = "获取请假记录")
    public RespPageBean getAllLeaveByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                          @RequestParam(defaultValue = "10") Integer size,
                                          AttdRecord attdRecord) {
        return attdRecordService.getLeaveByPage(currentPage, size, attdRecord);
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

    @PutMapping("/")
    @ApiModelProperty(value = "更新请假进度")
    public RespBean updateLeave(@RequestBody AttdRecord attdRecord) {
        if (attdRecordService.updateById(attdRecord)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

}
