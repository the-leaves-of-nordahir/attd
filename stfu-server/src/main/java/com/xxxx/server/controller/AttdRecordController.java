package com.xxxx.server.controller;

import com.xxxx.server.pojo.AttdRecord;
import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespPageBean;
import com.xxxx.server.service.IAttdRecordService;
import com.xxxx.server.service.ICourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 考勤管理
 *
 * @author: STFU
 * @create: 2021-04-29
 **/
@RestController
@RequestMapping("/attdmanagement/record")
public class AttdRecordController {

    @Autowired
    private IAttdRecordService attdRecordService;
    @Autowired
    private ICourseService courseService;

    @GetMapping("/")
    @ApiOperation(value = "获取考勤记录(分页)")
    public RespPageBean getRecordsByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                         @RequestParam(defaultValue = "10") Integer size,
                                         AttdRecord attdRecord) {
        return attdRecordService.getRecordsByPage(currentPage, size, attdRecord);
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

}
