package com.xxxx.server.controller;

import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.StfuDepartment;
import com.xxxx.server.pojo.Teacher;
import com.xxxx.server.service.ICourseService;
import com.xxxx.server.service.IStfuDepartmentService;
import com.xxxx.server.service.ITeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-21
 **/
@RestController
@RequestMapping("/edumanagement/teacher")
public class EduTeacherManagementController {

    @Autowired
    private ITeacherService teacherService;
    @Autowired
    private ICourseService courseService;
    @Autowired
    private IStfuDepartmentService stfuDepartmentService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有老师")
    public List<Teacher> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

    @PostMapping("/")
    @ApiOperation(value = "添加老师")
    public RespBean addTeacher(@RequestBody Teacher teacher) {
        if (teacherService.save(teacher)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/")
    @ApiOperation(value = "更新老师")
    public RespBean updateTeacher(@RequestBody Teacher teacher) {
        if (teacherService.updateById(teacher)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除老师")
    public RespBean deleteStudent(@PathVariable Integer id) {
        if (teacherService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @GetMapping("/department")
    @ApiOperation(value = "获取所有学院")
    public List<StfuDepartment> getAllDepartments() {
        return stfuDepartmentService.list();
    }

    @GetMapping("/course")
    @ApiOperation(value = "获取所有课程")
    public List<Course> getAllCourses() {
        return courseService.list();
    }

}
