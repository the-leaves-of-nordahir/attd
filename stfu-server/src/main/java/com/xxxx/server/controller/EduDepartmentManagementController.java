package com.xxxx.server.controller;

import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.StfuDepartment;
import com.xxxx.server.service.IStfuDepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-20
 **/
@RestController
@RequestMapping("/edumanagement/department")
public class EduDepartmentManagementController {

    @Autowired
    private IStfuDepartmentService departmentService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有学院")
    public List<StfuDepartment> getAllDepartments() {
        return departmentService.list();
    }

    @PostMapping("/")
    @ApiOperation(value = "添加学院")
    public RespBean addDepartment(@RequestBody StfuDepartment department) {
        if (departmentService.save(department)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/")
    @ApiOperation(value = "更新学院")
    public RespBean updateDepartment(@RequestBody StfuDepartment department) {
        if (departmentService.updateById(department)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除学院")
    public RespBean deleteDepartment(@PathVariable Integer id) {
        if (departmentService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @ApiOperation(value = "批量删除学院")
    @DeleteMapping("/")
    public RespBean deleteDepartmentsByIds(Integer[] ids) {
        if(departmentService.removeByIds(Arrays.asList(ids))) {
            return RespBean.success("批量删除成功！");
        }
        return RespBean.error("批量删除失败！");
    }

}
