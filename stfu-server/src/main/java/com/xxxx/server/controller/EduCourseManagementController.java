package com.xxxx.server.controller;

import com.xxxx.server.pojo.Course;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.pojo.Teacher;
import com.xxxx.server.service.ICourseService;
import com.xxxx.server.service.ITeacherCourseService;
import com.xxxx.server.service.ITeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: STFU
 * @create: 2021-04-21
 **/
@RestController
@RequestMapping("/edumanagement/course")
public class EduCourseManagementController {

    @Autowired
    private ICourseService courseService;
    @Autowired
    private ITeacherCourseService teacherCourseService;
    @Autowired
    private ITeacherService teacherService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有课程")
    public List<Course> getAllCourses() {
        return courseService.list();
    }

    @GetMapping("/teacher/{id}")
    @ApiOperation(value = "根据课程id获取课程对应老师列表")
    public List<Teacher> getTeachersByCourseId(@PathVariable Integer id) {
        return teacherService.getTeachersByCourseId(id);
    }

    @PostMapping("/")
    @ApiOperation(value = "添加课程")
    public RespBean addCourse(@RequestBody Course course) {
        if (courseService.save(course)) {
            return RespBean.success("添加成功！");
        }
        return RespBean.error("添加失败！");
    }

    @PutMapping("/teachers")
    @ApiOperation(value = "更新或添加课程与老师的关联")
    public RespBean updateCourseWithTeachers(Integer courseId, Integer[] teacherIds) {
        return teacherCourseService.updateCourseWithTeachers(courseId, teacherIds);
    }

    @PutMapping("/")
    @ApiOperation(value = "更新课程")
    public RespBean updateCourse(@RequestBody Course course) {
        if (courseService.updateById(course)) {
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除课程")
    public RespBean deleteCourse(@PathVariable Integer id) {
        if (courseService.removeById(id)) {
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

}
