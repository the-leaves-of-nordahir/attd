package com.xxxx.server.controller;

import com.xxxx.server.pojo.*;
import com.xxxx.server.pojo.result.RespBean;
import com.xxxx.server.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 老师_考勤管理
 *
 * @author: STFU
 * @create: 2021-04-14
 **/
@RestController
@RequestMapping("/attdmanagement/attendance")
public class AttdAttendanceController {

    @Autowired
    private IAiFaceService aiFaceService;
    @Autowired
    private IClassesService classesService;
    @Autowired
    private ICourseService courseService;
    @Autowired
    private ITeacherService teacherService;
    @Autowired
    private IStudentService studentService;
    @Autowired
    private IStfuDepartmentService stfuDepartmentService;
    @Autowired
    private IAttdRecordService attdRecordService;
    @Autowired
    private IMajorService majorService;

    @PostMapping("/multisearch")
    @ApiOperation(value = "M:N人脸搜索")
    public RespBean multiSearch(@RequestBody Image image) {
        if (!StringUtils.isEmpty(image.getImgStr())) {
            return aiFaceService.multiSearch(image);
        } else {
            return RespBean.error("请上传照片！");
        }
    }

    @GetMapping("/face/{id}")
    @ApiOperation(value = "获取用户人脸列表接口")
    public RespBean faceGetList(@PathVariable String id) {
        return aiFaceService.faceGetList(id);
    }

    @PostMapping("/attdrecord")
    @ApiOperation(value = "添加考勤记录")
    public RespBean addAttdRecords(@RequestBody List<AttdRecord> attdRecords) {
        if (attdRecordService.saveBatch(attdRecords)) {
            return RespBean.success("考勤记录添加成功！");
        }
        return RespBean.error("考勤记录添加失败！");
    }

    @GetMapping("/course/{id}")
    @ApiOperation(value = "根据老师id获取老师关联的课程")
    public List<Course> getCoursesByTeacherId(@PathVariable Integer id) {
        return courseService.getCoursesByTeacherId(id);
    }

    @GetMapping("/student/{id}")
    @ApiOperation(value = "根据班级id获取所有学生")
    public List<Student> getStudentsByClassesId(@PathVariable Integer id) {
            return studentService.getStudentsByClassesId(id);
    }

    @GetMapping("/student")
    @ApiOperation(value = "获取所有学生")
    public List<Student> getAllStudents() {
        return studentService.getStudentsByClassesId();
    }

    @GetMapping("/attdrecord")
    @ApiOperation(value = "获取所有考勤记录")
    public List<AttdRecord> getAllAttdRecords() {
        return attdRecordService.list();
    }

    @GetMapping("/course")
    @ApiOperation(value = "获取所有课程列表")
    public List<Course> getAllCourses() {
        return courseService.list();
    }

    @GetMapping("/classes")
    @ApiOperation(value = "获取所有班级列表")
    public List<Classes> getAllClasses() {
        return classesService.list();
    }

    @GetMapping("/department")
    @ApiOperation(value = "获取所有学院")
    public List<StfuDepartment> getAllDepartments() {
        return stfuDepartmentService.list();
    }

    @GetMapping("/major")
    @ApiOperation(value = "获取所有专业")
    public List<Major> getAllMajors() {
        return majorService.list();
    }

}
